import axios from 'axios';

import { messages } from '../config/constants';

export const ACTION_TYPES = {
  FETCH_TICKETS: 'tickets/FETCH_TICKETS',
  FETCH_TICKET: 'tickets/FETCH_TICKET',
  CLEAR_TICKET: 'tickets/CLEAR_TICKET',
  CREATE_TICKET: 'tickets/CREATE_TICKET',
  UPDATE_TICKET: 'tickets/UPDATE_TICKET',
  DELETE_TICKET: 'tickets/DELETE_TICKET'
};

const apiUrl = '/api/tickets';
// Actions
export const getTickets = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_TICKETS,
  payload: axios.get(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getTicket = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_TICKET,
    payload: axios.get(requestUrl)
  };
};

export const clearTicket = () => {
  return {
    type: ACTION_TYPES.CLEAR_TICKET
  };
};

export const createTicket = ticket => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_TICKET,
    meta: {
      successMessage: messages.DATA_CREATE_SUCCESS_ALERT,
      errorMessage: messages.DATA_UPDATE_ERROR_ALERT
    },
    payload: axios.post(apiUrl, ticket)
  });
  dispatch(getTickets());
  return result;
};

export const updateTicket = ticket => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_TICKET,
    meta: {
      successMessage: messages.DATA_CREATE_SUCCESS_ALERT,
      errorMessage: messages.DATA_UPDATE_ERROR_ALERT
    },
    payload: axios.put(apiUrl, ticket)
  });
  dispatch(getTickets());
  return result;
};

export const updateTicketStatus = request => async dispatch => {
  const requestUrl = `${apiUrl}/status`;
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_TICKET,
    meta: {
      successMessage: messages.DATA_CREATE_SUCCESS_ALERT,
      errorMessage: messages.DATA_UPDATE_ERROR_ALERT
    },
    payload: axios.put(requestUrl, request)
  });
  dispatch(getTickets());
  return result;
};

export const deleteTicket = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_TICKET,
    meta: {
      successMessage: messages.DATA_DELETE_SUCCESS_ALERT,
      errorMessage: messages.DATA_UPDATE_ERROR_ALERT
    },
    payload: axios.delete(requestUrl)
  });
  dispatch(getTickets());
  return result;
};
