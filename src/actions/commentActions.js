import axios from 'axios';

import { messages } from '../config/constants';

export const ACTION_TYPES = {
  FETCH_COMMENTS: 'comment/FETCH_COMMENTS',
  FETCH_COMMENT: 'comment/FETCH_COMMENT',
  CLEAR_COMMENT: 'comment/CLEAR_COMMENT',
  CREATE_COMMENT: 'comment/CREATE_COMMENT',
  UPDATE_COMMENT: 'comment/UPDATE_COMMENT',
  DELETE_COMMENT: 'comment/DELETE_COMMENT'
};

const apiUrl = '/api/tickets/comments';
const apiTicketsUrl = '/api/tickets';
const apiCommentsPath = '/comments';
// Actions
export const getTicketComments = (ticketId, page, size, sort) => ({
  type: ACTION_TYPES.FETCH_COMMENTS,
  payload: axios.get(`${apiTicketsUrl}/${ticketId}${apiCommentsPath}?cacheBuster=${new Date().getTime()}`)
});

export const createTicketComment = commentRequest => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_COMMENT,
    meta: {
      successMessage: messages.DATA_CREATE_SUCCESS_ALERT,
      errorMessage: messages.DATA_UPDATE_ERROR_ALERT
    },
    payload: axios.post(apiUrl, commentRequest)
  });
  dispatch(getTicketComments(commentRequest.ticketId));
  return result;
};

export const getComments = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_COMMENTS,
  payload: axios.get(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getComment = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_COMMENT,
    payload: axios.get(requestUrl)
  };
};

export const clearComment = () => {
  return {
    type: ACTION_TYPES.CLEAR_COMMENT
  };
};

export const createComment = comment => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_COMMENT,
    meta: {
      successMessage: messages.DATA_CREATE_SUCCESS_ALERT,
      errorMessage: messages.DATA_UPDATE_ERROR_ALERT
    },
    payload: axios.post(apiUrl, comment)
  });
  dispatch(getComments());
  return result;
};

export const updateComment = comment => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_COMMENT,
    meta: {
      successMessage: messages.DATA_CREATE_SUCCESS_ALERT,
      errorMessage: messages.DATA_UPDATE_ERROR_ALERT
    },
    payload: axios.put(apiUrl, comment)
  });
  dispatch(getComments());
  return result;
};

export const deleteComment = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_COMMENT,
    meta: {
      successMessage: messages.DATA_DELETE_SUCCESS_ALERT,
      errorMessage: messages.DATA_UPDATE_ERROR_ALERT
    },
    payload: axios.delete(requestUrl)
  });
  dispatch(getComments());
  return result;
};
