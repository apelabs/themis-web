import * as React from 'react';
import SelectField from 'react-md/lib/SelectField';
import { Field } from 'redux-form'

const renderSelectField = ({ input, meta: { touched, error }, ...others }) => (
    <SelectField {...input} {...others} error={touched && !!error} errorText={error} />
);

const SelectInput = (props) => (
    <Field
        id={props.id}
        name={props.name}
        label={props.label}
        helpText={props.id}
        component={renderSelectField}
        required={props.required}
        className="md-cell"
    />
  );
  
export default SelectInput;