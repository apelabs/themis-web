import * as React from 'react';
import './badge.css';

const getClassNameForStyle = (style) => {
    switch (style) {
        case "primary":
            return "badge-primary";
        case "danger":
            return "badge-danger";
        default:
            return "badge-default";
        }
};

const Badge = (props) => (
    <span className={"badge md-paper md-paper--1 " + getClassNameForStyle(props.style)}>{props.label}</span>
  );
  
export default Badge;