import * as React from 'react';
import { Avatar, Chip } from 'react-md';

const UserAvatar = (props) => (
    props.user ? 
    (
      props.iconOnly ? 
      <Avatar suffix="orange">{props.user.firstName[0] + props.user.lastName[0]}</Avatar>
      :
      <Chip
      label={props.user.login}
      avatar={<Avatar suffix="orange">{props.user.firstName[0] + props.user.lastName[0]}</Avatar>}/>    
    )
    :
    null    
  );
  
export default UserAvatar;