import './footer.scss';

import * as React from 'react';
import { Grid } from 'react-md';

const Footer = props => (
  <Grid {...props} id="main-footer" component="footer" className='footer'>
    <p>&copy; Themis 2017</p>
  </Grid>
    
);

export default Footer;
