import * as React from 'react';

import {FontIcon, ListItem} from 'react-md';
import {Link, Route} from 'react-router-dom';

export class NavLink extends React.Component {

    render() {
        const { label, to, exact, icon  } = this.props;
        return (
            <Route path={to} exact={exact}>
            {({ match }) => {
              let leftIcon;
              if (icon) {
                leftIcon = <FontIcon>{icon}</FontIcon>;
              }
              return (
                <ListItem
                  component={Link}
                  active={!!match}
                  to={to}
                  primaryText={label}
                  leftIcon={leftIcon}
                />
              );
            }}
          </Route>          
        )
    }

}

export default NavLink;