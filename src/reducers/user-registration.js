import axios from 'axios';

import { REQUEST, SUCCESS, FAILURE } from './action-type.util';

export const ACTION_TYPES = {
  REGISTER: 'authentication/REGISTER',
  ERROR_MESSAGE: 'authentication/ERROR_MESSAGE'
};

const initialState = {
  loading: false,
  isRegistered: false,
  errorMessage: null, // Errors returned from server side
  registerError: false // Errors returned from server side
};

// Reducer

export default (state = initialState, action) => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.REGISTER):
      return {
        ...state,
        loading: true
      };
    case FAILURE(ACTION_TYPES.REGISTER):
      return {
        ...initialState,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.REGISTER):
      return {
        ...state,
        loading: false,
        isRegistered: true
      };
    default:
      return state;
  }
};

export const register = (user) => async (dispatch, getState) => {
  const result = await dispatch({
    type: ACTION_TYPES.REGISTER,
    payload: axios.post('/api/register', { ...user })
  });
};