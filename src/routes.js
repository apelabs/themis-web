import * as React from 'react';
import { Route } from 'react-router-dom';

import PrivateRoute from './shared/layout/private-route/private-route';
import Login from './components/login/login';
import Register from './components/login/register';
import Logout from './components/login/logout';
import Home from './components/home/home';
import Admin from './components/administration';
import Account from './components/account';
import Tickets from './components/tickets';

const Routes = () => (
  <div>
    <Route exact path="/" component={Home}/>
    <Route path="/login" component={Login} />
    <Route path="/register" component={Register} />
    <Route path="/logout" component={Logout} />
    <PrivateRoute path="/admin" component={Admin} />
    <PrivateRoute path="/account" component={Account} />
    <PrivateRoute path="/tickets" component={Tickets} />
  </div>
);

export default Routes;
