import './home.scss';

import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Cell,
  Button,
  Card,
  CardActions,
  CardTitle,
  CardText,
  Media,
  MediaOverlay,
} from 'react-md';
import HomeHero from './home-hero';
import TicketsDashboard from '../tickets/tickets-dashboard'
import { getSession } from '../../reducers/authentication';
import transparent from '../../images/transparent.jpg';
import secure from '../../images/secure.jpg';
import accessible from '../../images/accessible.jpg';


export class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUser: props.account
    };
  }

  componentWillMount() {
    this.props.getSession();
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      currentUser: nextProps.account
    });
  }

  render() {
    const { currentUser } = this.state;

    return (
      (currentUser && currentUser.login) ? (
        <TicketsDashboard match={this.props.match}/>
      )
      :
      (
        <div className="md-grid">
        <HomeHero currentUser={currentUser}/>
        <Card className="card-home md-cell md-cell--4 md-cell--4-tablet">
          <Media>
            <img src={secure} alt="secure" />
            <MediaOverlay>
              <CardTitle title="Secure" subtitle="Lorem ipsum dolor sit amet">
              </CardTitle>
            </MediaOverlay>
          </Media>
          <CardText>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut eleifend odio.
              Vivamus quis quam eget augue facilisis laoreet. Aliquam egestas turpis pellentesque
              Sed elementum, risus eget fermentum accumsan, nunc ante commodo diam, eget pulvinar
              risus velit eu sapien. Nunc vitae pellentesque nisl.
            </p>
          </CardText>
        </Card>
        <Card className="card-home md-cell md-cell--4 md-cell--4-tablet">
          <Media>
            <img src={transparent} alt="transparent" />
            <MediaOverlay>
              <CardTitle title="Transparent" subtitle="Mauris eu urna at arcu">
              </CardTitle>
            </MediaOverlay>
          </Media>
          <CardText>
            <p>
              Maecenas lacinia enim ut risus pellentesque euismod. Vestibulum gravida, risus non
              condimentum volutpat, orci elit laoreet elit, in auctor eros orci non quam. Proin ut
              tellus et est dignissim efficitur. Aliquam erat volutpat. Proin pellentesque metus
              sit amet libero auctor aliquet. Donec scelerisque erat in magna sagittis hendrerit.
              Sed pulvinar enim mattis mauris sodales semper. Mauris eu urna at arcu dapibus
            </p>
          </CardText>
        </Card>
        <Card className="card-home md-cell md-cell--4 md-cell--4-tablet">
          <Media>
            <img src={accessible} alt="accessible" />
            <MediaOverlay>
              <CardTitle title="Accessible" subtitle="Sed pulvinar enim mattis">
              </CardTitle>
            </MediaOverlay>
          </Media>
          <CardText>
            <p>
              Proin pellentesque metus sit amet libero auctor aliquet. Donec scelerisque erat in magna sagittis hendrerit.
              Sed pulvinar enim mattis mauris sodales semper. Mauris eu urna at arcu dapibus
            </p>
          </CardText>
        </Card>
      </div>
      )
    );
  }
}

const mapStateToProps = storeState => ({
  account: storeState.authentication.account,
  isAuthenticated: storeState.authentication.isAuthenticated
});

const mapDispatchToProps = { getSession };

export default connect(mapStateToProps, mapDispatchToProps)(Home);
