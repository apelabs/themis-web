import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Cell,
  Button,
  Card,
  CardActions,
  CardTitle,
  CardText,
  Media,
  MediaOverlay,
} from 'react-md';

import themis from '../../images/welcome-google.jpg';

const HomeHero = ({ currentUser }) => (
      <Cell size={6} tabletOffset={1} desktopOffset={3}>
        <Card className="card-home">
          <Media>
            <img src={themis} alt="themis" />
            <MediaOverlay>
              <CardTitle title="Welcome to Themis">
              </CardTitle>
            </MediaOverlay>
          </Media>
          <CardText>
          {
            (currentUser && currentUser.login) ? (
              <p>
                  You are logged in as {currentUser.login}
              </p>
            ) : (
              <p>
                <p className="alert alert-warning">
                  If you want to
                  <Link to="/login"> sign in</Link>
                  , you can try the default accounts:
                  <br />- Administrator (login='admin' and password='admin')
                  <br />- User (login='user' and password='user').
                </p>

                <p>
                  You do not have an account yet?&nbsp;
                  <a className="alert-link">Register a new account</a>
                </p>
              </p>
            )
          }
          </CardText>
          <CardActions className="md-divider-border md-divider-border--top">
          {
            (currentUser && currentUser.login) ? (  
              <div>  
                <Button component={Link} to="/tickets" raised primary>Tickets</Button>
                <Button component={Link} to="/logout" flat>Logout</Button>   
              </div>    
            ) : (
              <div>           
                <Button component={Link} to="/login" flat primary>Login</Button>
                <Button component={Link} to="/register" flat>Register</Button>
              </div>
            )  
          }          
          </CardActions>
        </Card>
      </Cell>
);

export default HomeHero;
