import React from 'react';
import { List, Subheader, ListItem } from 'react-md';
import { Form, Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import UserAvatar from '../../shared/common/UserAvatar';
import TextInput from '../../shared/form-fields/TextInput';
import TextAreaInput from '../../shared/form-fields/TextAreaInput';
import moment from 'moment';

const formatDate = (date) => {
  return moment(date).fromNow();
};

let CommentsList = ({comments}) => {
  if (!comments) {
    return null;
  }
  return (
    <List>
    <Subheader primaryText="Comments" primary />
    {comments.map((comment, i) => (
        <ListItem
          className="ticket-comment"
          leftAvatar={<UserAvatar user={comment.user} iconOnly={true}/>}
          primaryText={comment.user.firstName + ": " + formatDate(comment.createdDate)}
          secondaryText={comment.comment}
          rightIcon={comment.commentType}
          threeLines
        />
        ))}
    </List>
  )
}

export default CommentsList;
