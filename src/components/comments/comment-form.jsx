import React from 'react';
import { Cell, CardActions, CardText, Button, TextField } from 'react-md';
import { Form, Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import TextInput from '../../shared/form-fields/TextInput';
import TextAreaInput from '../../shared/form-fields/TextAreaInput';

let CommentForm = ({initialValues, handleSubmit, handleClose, handleSave, loginError, title}) => {

  return (
    <Form id="comment-form" onSubmit={handleSubmit(handleSave)}>
      <TextAreaInput 
        id="comment" 
        name="comment" 
        label="Comment" 
        required />  
      <div className="md-cell--right align-right">
        <Button raised type="submit">Submit</Button>
      </div>        
    </Form> 
  )
}

CommentForm = reduxForm({
  form: 'comment-form'
})(CommentForm)

export default CommentForm;
