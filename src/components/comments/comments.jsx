import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { MdAdd, MdRemoveRedEye, MdEdit, MdDelete } from 'react-icons/lib/md';
import {
  Card,
  Button,
  CardTitle,
  DataTable,
  TableHeader,
  TableBody,
  TableRow,
  TableColumn,
  Chip,
  Avatar
} from 'react-md';
import moment from 'moment';
import { getTicketComments, createTicketComment } from '../../actions/commentActions';
import CommentForm from './comment-form';
import CommentsList from './comments-list';

const formatDate = (date) => {
  return moment(date).fromNow();
};

export class Comments extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    if (this.props.ticketId != null){
      this.props.getTicketComments(this.props.ticketId);
    }    
  }

  saveComment = (values) => {
    let commentRequest = values;
    commentRequest.ticketId = this.props.ticketId;
    this.props.createTicketComment(commentRequest);
  }

  render() {
    const { tickets, account, match } = this.props;
    return (
      <div className="md-full-width">
        <CommentForm handleSave={this.saveComment} />
        <CommentsList comments={this.props.comments} />
      </div>
    );
  }
}

const mapStateToProps = storeState => ({
  comments: storeState.comment.comments,
  ticketId: storeState.ticket.ticket.id,
  account: storeState.authentication.account
});

const mapDispatchToProps = { getTicketComments, createTicketComment };

export default connect(mapStateToProps, mapDispatchToProps)(Comments);
