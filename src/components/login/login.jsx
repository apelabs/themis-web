import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import LoginForm from './login-form';
import { login } from '../../reducers/authentication';

export class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showModal: !props.isAuthenticated,
      redirectToReferrer: props.isAuthenticated
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      showModal: !nextProps.isAuthenticated,
      redirectToReferrer: nextProps.isAuthenticated
    });
  }

  handleLogin = (values) => {
    const { username, password, rememberMe} = values;
    this.props.login(username, password, false);
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/', search: this.props.location.search } };
    const { redirectToReferrer } = this.state;
    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <LoginForm
        onSubmit={this.handleLogin}
        loginError={this.props.loginError}
      />
    );
  }
}

const mapStateToProps = storeState => ({
  isAuthenticated: storeState.authentication.isAuthenticated,
  loginError: storeState.authentication.loginError
});

const mapDispatchToProps = { login };

export default connect(mapStateToProps, mapDispatchToProps)(Login);
