import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import RegisterForm from './register-form';
import { register } from '../../reducers/user-registration';

export class Register extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      redirectToReferrer: props.isAuthenticated
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      redirectToReferrer: nextProps.isAuthenticated
    });
  }

  handleRegister = (values) => {
    this.props.register(values);
  }

  render() {
    const { from } = this.props.location.state || { from: { pathname: '/', search: this.props.location.search } };
    const { redirectToReferrer } = this.state;
    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }
    return (
      <RegisterForm
        onSubmit={this.handleRegister}
        error={this.props.errorMessage}
        isRegistered={this.props.isRegistered}
        loading={this.props.loading}
      />
    );
  }
}

const mapStateToProps = storeState => ({
  isAuthenticated: storeState.authentication.isAuthenticated,
  isRegistered: storeState.userRegistration.isRegistered,
  errorMessage: storeState.userRegistration.errorMessage,
  loading: storeState.userRegistration.loading
});

const mapDispatchToProps = { register };

export default connect(mapStateToProps, mapDispatchToProps)(Register);
