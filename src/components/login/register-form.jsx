import React from 'react';
import { Cell, Card, CardTitle, CardActions, CardText, Button, TextField } from 'react-md';
import { Form, Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import TextInput from '../../shared/form-fields/TextInput'

let RegisterForm = ({handleSubmit, isRegistered, loading, error}) => {
  
  if (isRegistered) {
    return (
      <Cell size={6} tabletOffset={1} desktopOffset={2}>
        <Card className="md-block-centered"> 
          <CardTitle
                  title="Registration"
                />    
          <CardText>
            Registered succesfully! Please check your email for activation,
          </CardText>
          <CardActions className="md-divider-border md-divider-border--top">
              <Button component={Link} to="/" raised primary>Home</Button>
          </CardActions>  
        </Card>
      </Cell>
    )
  }

  return (
    <Form id="login-form" onSubmit={handleSubmit}>
      <Cell size={6} tabletOffset={1} desktopOffset={2}>
        <Card className="md-block-centered">        
            <CardTitle
              title="Registration"
              subtitle="Submit the form to register"
            />
             <CardText>
                { error ?
                  <div className="alert alert-danger">
                    Error with Registration: {error}
                  </div>
                  : null
                }
                  <div>
                    <TextInput
                      name="firstName"
                      label="First Name"
                      placeholder="First Name"
                      errorText="FirstName cannot be empty!"
                    />
                    <TextInput
                      name="lastName"
                      label="Last Name"
                      placeholder="Last Name"
                      required errorText="First Name cannot be empty!"
                    />
                    <TextInput
                      name="email"
                      label="Email"
                      placeholder="abc@xyz.com"
                      required errorText="First Name cannot be empty!"
                    />
                    <TextInput
                      name="login"
                      label="Username"
                      placeholder="Username"
                      required errorText="Username cannot be empty!"
                    />
                    <TextInput
                      type="password"
                      name="password"
                      label="Password"
                      placeholder="password"
                      required errorText="Password cannot be empty!"
                    />
                  </div>
                </CardText>

            <CardActions className="md-divider-border md-divider-border--top">
              <Button type="submit" raised primary>Register</Button>
              <Button component={Link} to="/" flat>Cancel</Button>
            </CardActions>        
        </Card>
      </Cell>   
    </Form> 
  )
}

RegisterForm = reduxForm({
  form: 'register-form'
})(RegisterForm)

export default RegisterForm;
