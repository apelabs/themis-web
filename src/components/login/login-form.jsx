import React from 'react';
import { Cell, Card, CardTitle, CardActions, CardText, Button, TextField } from 'react-md';
import { Form, Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import TextInput from '../../shared/form-fields/TextInput'

let LoginForm = ({handleSubmit, handleLogin, loginError}) => {

  return (
    <Form id="login-form" onSubmit={handleSubmit}>
      <Cell size={4} tabletOffset={2} desktopOffset={4}>
        <Card className="md-block-centered">        
            <CardTitle
              title="Login"
              subtitle="Login using your credentials"
            />
            <CardText>
              <div className="row">
                <div className="col-md-12">
                  { loginError ?
                    <div className="alert alert-danger">
                      Username or passsword not correct
                    </div>
                    : null
                  }
                </div>
                <div className="col-md-12">
                    <TextInput
                      name="username"
                      label="Username"
                      placeholder="username"
                      required errorText="Username cannot be empty!"
                    />
                    <TextInput
                      type="password"
                      name="password"
                      label="Password"
                      placeholder="password"
                      required errorText="Password cannot be empty!"
                    />
                </div>
              </div>
            </CardText>
            <CardActions className="md-divider-border md-divider-border--top">
              <Button type="submit" raised primary>Login</Button>
              <Button component={Link} to="/" flat>Cancel</Button>
            </CardActions>        
        </Card>
      </Cell>   
    </Form> 
  )
}

LoginForm = reduxForm({
  form: 'login-form'
})(LoginForm)

export default LoginForm;
