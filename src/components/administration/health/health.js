import * as React from 'react';
import { connect } from 'react-redux';
import { Table } from 'reactstrap';
import { MdRemoveRedEye, MdRefresh } from 'react-icons/lib/md';

import HealthModal from './health-modal';
import { systemHealth } from '../../../reducers/administration';

export class HealthPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      healthObject: {},
      showModal: false
    };
  }

  componentDidMount() {
    this.props.systemHealth();
  }

  getSystemHealth = () => {
    if (!this.props.isFetching) {
      this.props.systemHealth();
    }
  }

  getSystemHealthInfo = (name, healthObject) => {
    this.setState({
      showModal: true,
      healthObject: {
        ...healthObject,
        name
      }
    });
  }

  handleClose = () => {
    this.setState({
        showModal: false
    });
  }

  renderModal = () => {
    const { healthObject } = this.state;
    return (
      <HealthModal healthObject={healthObject} handleClose={this.handleClose} showModal={this.state.showModal} />
    );
  }

  render() {
    const { health, isFetching } = this.props;
    const data = health || {};
    return (
      <div>
          <h2>Health Checks</h2>
          <p>
            <button type="button" onClick={this.getSystemHealth} className={isFetching ? 'btn btn-danger' : 'btn btn-primary'} disabled={isFetching}>
              <MdRefresh />&nbsp;
              Refresh
            </button>
          </p>
          <div className="row">
            <div className="col-12">
            <Table bordered>
               <thead>
                 <tr>
                   <th>Service Name</th>
                   <th>Status</th>
                   <th>Details</th>
                 </tr>
               </thead>
               <tbody>
              {Object.keys(data).map((configPropKey, configPropIndex) =>
                (configPropKey !== 'status') ?
                  <tr key={configPropIndex}>
                    <td>{configPropKey}</td>
                    <td><button type="button" className={data[configPropKey].status !== 'UP' ? 'btn btn-danger' : 'btn btn-success'}>{data[configPropKey].status}</button></td>
                    <td><a onClick={this.getSystemHealthInfo(configPropKey, data[configPropKey])}><MdRemoveRedEye /></a></td>
                  </tr>
                : null
                )}
              </tbody>
             </Table>
            </div>
          </div>
          {this.renderModal()}
        </div>
    );
  }
}

const mapStateToProps = storeState => ({
  health: storeState.administration.health,
  isFetching: storeState.administration.isFetching
});

const mapDispatchToProps = { systemHealth };

export default connect(mapStateToProps, mapDispatchToProps)(HealthPage);
