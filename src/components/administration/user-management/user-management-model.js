import * as React from 'react';
import { connect } from 'react-redux';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Label } from 'reactstrap';
import { MdBlock, MdSave } from 'react-icons/lib/md';

import { getUser, getRoles, updateUser, createUser } from '../../../reducers/user-management';

export class UserManagementModel extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showModal: true,
      isNew: !this.props.match.params || !this.props.match.params.login
    };
  }

  componentDidMount() {
    !this.state.isNew && this.props.getUser(this.props.match.params.login);
    this.props.getRoles();
  }

  saveUSer = (event, errors, values) => {
    if (this.state.isNew) {
      this.props.createUser(values);
    } else {
      this.props.updateUser(values);
    }
    this.handleClose();
  }

  handleClose = () => {
    this.setState({
        showModal: false
    });
    this.props.history.push('/admin/user-management');
  }

  render() {
    const isInvalid = false;
    const { user, loading, updating, roles } = this.props;
    const { showModal, isNew } = this.state;
    return (
      <Modal
        isOpen={showModal} modalTransition={{ timeout: 20 }} backdropTransition={{ timeout: 10 }}
        toggle={this.handleClose} size="lg"
      >

    </Modal>
    );
  }
}

const mapStateToProps = storeState => ({
  user: storeState.userManagement.user,
  roles: storeState.userManagement.authorities,
  loading: storeState.userManagement.loading,
  updating: storeState.userManagement.updating
});

const mapDispatchToProps = { getUser, getRoles, updateUser, createUser };

export default connect(mapStateToProps, mapDispatchToProps)(UserManagementModel);
