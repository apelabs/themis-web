import * as React from 'react';
import { connect } from 'react-redux';

import { getAudits } from '../../../reducers/administration';

export class AuditsPage extends React.Component {

  componentDidMount() {
    this.props.getAudits();
  }

  getAuditList = () => {
    if (!this.props.isFetching) {
      this.props.getAudits();
    }
  }

  render() {
    const { audits } = this.props;
    return (
      <div>
          <h2>Audits</h2>
          FIX ME pagination and filter by date and sorting
          <hr/>
          <div className="row">
            <div className="col-12">
              <table>
                <thead>
                  <tr>
                    <th>Timestamp</th>
                    <th>Principal</th>
                    <th>Address</th>
                    <th>Type</th>
                  </tr>
                </thead>
                <tbody>
                  {audits.map((row, index) => (
                    <tr key={index}>
                      <tr>{row.timestamp}</tr>
                      <tr>{row.principal}</tr>
                      <tr>{row.data.remoteAddress}</tr>
                      <tr>{row.type}</tr>
                    </tr>
                  ))}
                </tbody>
              </table>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  ({ administration }) => ({ audits: administration.audits, isFetching: administration.isFetching }),
  { getAudits }
)(AuditsPage);
