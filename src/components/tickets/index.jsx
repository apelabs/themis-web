
import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ModalRoute } from 'react-router-modal';

import Tickets from './tickets';
import TicketDetail from './ticket-detail';
import TicketModal from './ticket-modal';
import TicketDeleteModal from './ticket-delete-modal';

const Routes = ({ match }) => (
  <div>
    <Switch>
      <Route exact path={match.url} component={Tickets} />
      <Route exact parentPath={match.url} path={`${match.url}/new`} component={TicketModal} />
      <ModalRoute exact parentPath={match.url} path={`${match.url}/:id/delete`} component={TicketDeleteModal} />
      <Route exact parentPath={match.url} path={`${match.url}/:id/edit`} component={TicketModal} />
      <Route exact path={`${match.url}/:id`} component={TicketDetail} />
    </Switch>
  </div>
);

export default Routes;
