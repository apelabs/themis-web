import * as React from 'react';
import {
  Cell,
  Grid,
  Paper
} from 'react-md';
import * as statuses from '../../../shared/model/ticket-statuses'

import './ticket-status-bar.css';

const CURRENT_STATUS_CLASS_NAME = "ticket-status-cell-current";
const COMPLETED_STATUS_CLASS_NAME = "ticket-status-cell-completed";
const FUTURE_STATUS_CLASS_NAME = "ticket-status-cell-future";

const getStatusClass = (ticketStatus, cellStatus) => {
  if (ticketStatus === cellStatus) {
    return CURRENT_STATUS_CLASS_NAME
  }
  switch (cellStatus) {
    case statuses.PENDING:
      return COMPLETED_STATUS_CLASS_NAME;
    case statuses.INITIAL_INVESTIGATION:
      if (ticketStatus === statuses.PENDING) {
        return FUTURE_STATUS_CLASS_NAME;
      } else {
        return COMPLETED_STATUS_CLASS_NAME;
      }
    case statuses.LIABILITY:
      if (ticketStatus === statuses.PENDING || ticketStatus === statuses.INITIAL_INVESTIGATION) {
        return FUTURE_STATUS_CLASS_NAME;
      } else {
        return COMPLETED_STATUS_CLASS_NAME;
      }
    case statuses.DAMAGES_ASSESSMENT:
      if (ticketStatus === statuses.PENDING || ticketStatus === statuses.INITIAL_INVESTIGATION || ticketStatus === statuses.LIABILITY) {
        return FUTURE_STATUS_CLASS_NAME;
      } else {
        return COMPLETED_STATUS_CLASS_NAME;
      }
    case statuses.SETTLEMENT_HEARING:
      if (ticketStatus === statuses.PENDING || ticketStatus === statuses.INITIAL_INVESTIGATION || ticketStatus === statuses.LIABILITY || ticketStatus === statuses.DAMAGES_ASSESSMENT) {
        return FUTURE_STATUS_CLASS_NAME;
      } else {
        return COMPLETED_STATUS_CLASS_NAME;
      }
    case statuses.CLOSED:
        return FUTURE_STATUS_CLASS_NAME;
    default:
      return FUTURE_STATUS_CLASS_NAME;
  }
}

export class TicketStatusBar extends React.Component {

  render() {
    const { status } = this.props;
    return (
      <Grid className="grid-status" noSpacing>
            <Cell size={2} className={getStatusClass(status, statuses.PENDING)}>
              Pending
              </Cell>
            <Cell size={2} className={getStatusClass(status, statuses.INITIAL_INVESTIGATION)}>
              Initial Investigation
              </Cell>
            <Cell size={2} className={getStatusClass(status, statuses.LIABILITY)}>
              Liability
              </Cell>
            <Cell size={2} className={getStatusClass(status, statuses.DAMAGES_ASSESSMENT)}>
              Damages Assessmnet
              </Cell>
            <Cell size={2} className={getStatusClass(status, statuses.SETTLEMENT_HEARING)}>
              Settlement Hearing
              </Cell>
            <Cell size={2} className={getStatusClass(status, statuses.CLOSED)}>
              Closed
            </Cell>        
      </Grid>
    )
  }

}

export default TicketStatusBar;