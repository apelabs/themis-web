import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { MdArrowBack } from 'react-icons/lib/md';
import { TicketStatusBar } from './status/ticket-status-bar'
import {
  Avatar,
  Cell,
  Chip,
  Button,
  Grid,
  List,
  ListItem,
  Card,
  CardActions,
  CardTitle,
  CardText,
  Paper,
  DialogContainer,
  TabsContainer,
  Tabs,
  Tab,
  Subheader
} from 'react-md';
import Badge from '../../shared/common/Badge';
import UserAvatar from '../../shared/common/UserAvatar';
import moment from 'moment';
import * as statuses from '../../shared/model/ticket-statuses'
import Comments from '../comments/comments';
import { getTicket, updateTicketStatus, clearTicket } from '../../actions/ticketActions';

const formatStatus = (status) => {
  if ("PENDING" === status) {
      return <Badge label={status} style="primary"/>
  } else if ("CLOSED" === status) {
      return <Badge label={status} style="danger"/>
  } else {
      return <Badge label={status} style="default"/>
  }
};

const formatDate = (date) => {
  return moment(date).format('DD/MM/YYYY');
};



export class TicketDetail extends React.Component {

  state = { modalVisible: false };

  showModal = () => {
    this.setState({ modalVisible: true });
  };
  
  hideModal = () => {
    this.setState({ modalVisible: false });
  };

  getNextTicketStatus = () => {
    switch (this.props.ticket.status) {
      case statuses.PENDING: return statuses.INITIAL_INVESTIGATION;
      case statuses.INITIAL_INVESTIGATION: return statuses.LIABILITY;
      case statuses.LIABILITY: return statuses.DAMAGES_ASSESSMENT;
      case statuses.DAMAGES_ASSESSMENT: return statuses.SETTLEMENT_HEARING;
      case statuses.SETTLEMENT_HEARING: return statuses.CLOSED;
      default: return statuses.CLOSED;
    }
  };

  progressTicketStatus = () => {
    let request = {id: this.props.ticket.id, status: this.getNextTicketStatus(), comment: 'Test Comment'};
    this.props.updateTicketStatus(request);
    this.setState({ modalVisible: false });
  };

  modalActions = [{
    onClick: this.progressTicketStatus,
    primary: true,
    children: 'Ok',
  }, {
    onClick: this.hideModal,
    primary: true,
    children: 'Cancel',
  }];

  componentDidMount() {
    this.props.getTicket(this.props.match.params.id);
  }

  componentWillUnmount() {
    this.props.clearTicket();
  }

  render() {
    const { ticket } = this.props;
    if (this.props.loading) {
      return <div>loading ...</div>
    }
    return (
      <div>
        <Card className="card-ticket-main md-cell md-cell--12 md-block-centered">
          <CardTitle title={"Matter: " + ticket.name}>
            <div className="md-cell--right">
              <Button raised primary onClick={this.showModal}>Progess</Button>
              <Button raised secondary>Close</Button>
            </div>
          </CardTitle>
          
          <Grid className="card-ticket-grid">
          <Cell size={12}>
            <TicketStatusBar status={ticket.status} />
          </Cell>          
            <Cell size={6}>
              <List className="ticket-details-list">          
                <ListItem primaryText="Reference #" secondaryText={ticket.id} />
                <ListItem primaryText="Label" secondaryText={ticket.label} />
                <ListItem primaryText="Type" secondaryText={ticket.ticketType} />
              </List>
            </Cell>
            <Cell size={6}>
              <List className="ticket-details-list">          
                <ListItem primaryText="Status" rightIcon={formatStatus(ticket.status)} />
                <ListItem primaryText="Created By" secondaryText={ticket.createdBy ? <UserAvatar user={ticket.createdBy} /> : null} />
                <ListItem primaryText="Created Date" secondaryText={formatDate(ticket.createdDate)} />
                <ListItem primaryText="Last Modified By" secondaryText={ticket.lastModifiedUser ? ticket.lastModifiedUser.firstName : null} />
                <ListItem primaryText="Last Updated Date" secondaryText={formatDate(ticket.lastUpdatedDate)} />
              </List>
            </Cell>
          </Grid>
          <CardText>
            <List>
            <Subheader primaryText="Description" primary />
                <ListItem
                  primaryText=""
                  secondaryText={ticket.description}
                  threeLines
                />
            </List>
          </CardText>
          <CardActions className="md-divider-border md-divider-border--top">
            <Button
              component={Link} to="/tickets"
              primary flat
            >
            <MdArrowBack/> Back</Button>        
          </CardActions>
        </Card>
        <Card className="card-ticket-main md-cell md-cell--12 md-block-centered">
          <TabsContainer panelClassName="md-grid" colored>
            <Tabs tabId="simple-tab">
              <Tab label="Comments">
                <Comments ticketId={ticket.id} />
              </Tab>
              <Tab label="History">
                <h3>History ... Coming soon!</h3>
              </Tab>
            </Tabs>
          </TabsContainer>
        </Card>


        

        <DialogContainer
          id="dialogue"
          visible={this.state.modalVisible}
          title="Progress Status"
          onHide={this.hideModal}
          aria-describedby="dialogue-description"
          modal
          actions={this.modalActions}
        >
          <p id="dialogue-description" className="md-color--secondary-text">
            Confirm Progressing the status to: {this.getNextTicketStatus()}
          </p>
        </DialogContainer>
      </div>

    );
  }
}

const mapStateToProps = storeState => ({
  ticket: storeState.ticket.ticket,
  loading: storeState.ticket.loading
});

const mapDispatchToProps = { getTicket, updateTicketStatus, clearTicket };

export default connect(mapStateToProps, mapDispatchToProps)(TicketDetail);
