import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { MdAdd, MdRemoveRedEye, MdEdit, MdDelete } from 'react-icons/lib/md';
import {
  Card,
  Button,
  CardTitle,
  DataTable,
  TableHeader,
  TableBody,
  TableRow,
  TableColumn,
  Chip,
  Avatar
} from 'react-md';
import Badge from '../../shared/common/Badge';
import moment from 'moment';

import { ICrudGetAction } from '../../shared/model/redux-action.type';
import { getTickets } from '../../actions/ticketActions';

const formatStatus = (status) => {
  if ("PENDING" === status) {
      return <Badge label={status} style="primary"/>
  } else if ("CLOSED" === status) {
      return <Badge label={status} style="danger"/>
  } else {
      return <Badge label={status} style="default"/>
  }
  
};

const formatDate = (date) => {
  return moment(date).fromNow();
};

export class Tickets extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getTickets();
  }

  render() {
    const { tickets, account, match } = this.props;
    return (
      <Card>
        <CardTitle title="Tickets">
          <div className="md-cell--right">
            <Link to={`${match.url}/new`}>
              <Button floating primary>add</Button>
            </Link>
          </div>          
        </CardTitle>
        <DataTable plain>
            <TableHeader>
              <TableRow>
                <TableColumn>ID</TableColumn>
                <TableColumn>Name</TableColumn>
                <TableColumn>Status</TableColumn>
                <TableColumn>Assigned To</TableColumn>
                <TableColumn>Created Date</TableColumn>
                <TableColumn>Last Updated Date</TableColumn>
                <TableColumn></TableColumn>
              </TableRow>
            </TableHeader>
            <TableBody>
              {
              tickets.map((ticket, i) => (
                <TableRow key={`user-${i}`}>
                  <TableColumn>
                    <Button
                      component={Link} to={`${match.url}/${ticket.id}`}
                      flat
                    >
                      {ticket.id}
                    </Button>
                  </TableColumn>
                  <TableColumn>{ticket.name}</TableColumn>
                  <TableColumn>
                    {formatStatus(ticket.status)}
                  </TableColumn>
                  <TableColumn>{ticket.assigned_to_id}</TableColumn>
                  <TableColumn>{formatDate(ticket.createdDate)}</TableColumn>
                  <TableColumn>{formatDate(ticket.lastUpdatedDate)}</TableColumn>
                  <TableColumn>
                      <div className="buttons__group">
                        <Button
                          component={Link} to={`${match.url}/${ticket.id}`}
                          flat
                        >
                          <MdRemoveRedEye/> view
                        </Button>
                        <Button
                          component={Link} to={`${match.url}/${ticket.id}/edit`}
                          flat primary
                        >
                          <MdEdit/> edit
                        </Button>
                        <Button
                          component={Link} to={`${match.url}/${ticket.id}/delete`}
                          flat secondary
                        >
                          <MdDelete/> delete
                        </Button>
                     </div>
                  </TableColumn>
                </TableRow>
              ))
            }
            </TableBody>
        </DataTable>
      </Card>
    );
  }
}

const mapStateToProps = storeState => ({
  tickets: storeState.ticket.tickets,
  account: storeState.authentication.account
});

const mapDispatchToProps = { getTickets };

export default connect(mapStateToProps, mapDispatchToProps)(Tickets);
