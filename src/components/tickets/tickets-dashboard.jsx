import * as React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { MdAdd, MdRemoveRedEye, MdEdit, MdDelete } from 'react-icons/lib/md';
import {
  Grid,
  Cell,
  List,
  Subheader,
  Card,
  Button,
  CardTitle,
} from 'react-md';
import Badge from '../../shared/common/Badge';
import moment from 'moment';
import TicketDashboardCard from './ticket-dashboard-card';
import { ICrudGetAction } from '../../shared/model/redux-action.type';
import * as statuses from '../../shared/model/ticket-statuses'
import { getTickets } from '../../actions/ticketActions';

export class TicketsDashboard extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getTickets();
  }

  render() {
    const { tickets, account, match } = this.props;
    const pendingTickets = tickets.filter(ticket => ticket.status==statuses.PENDING);
    const investigationTickets = tickets.filter(ticket => ticket.status==statuses.INITIAL_INVESTIGATION);
    const liabilityTickets = tickets.filter(ticket => ticket.status==statuses.LIABILITY);
    const damagesTickets = tickets.filter(ticket => ticket.status==statuses.DAMAGES_ASSESSMENT);
    const settlementTickets = tickets.filter(ticket => ticket.status==statuses.SETTLEMENT_HEARING);
    const closedTickets = tickets.filter(ticket => ticket.status==statuses.CLOSED);
    return (
      <Grid>
        <Cell size={2} phoneSize={4}>
          <List>
            <Subheader primary primaryText="Pending" />
          </List>
          {
              pendingTickets.map((ticket, i) => (
              <TicketDashboardCard ticket={ticket} key={`ticket-${i}`} match={match} />
              ))
            }
        </Cell>
        <Cell size={2} phoneSize={4}>
          <List>
            <Subheader primary primaryText="Initial Investigation" />
          </List>
          {
              investigationTickets.map((ticket, i) => (
              <TicketDashboardCard ticket={ticket} key={`ticket-${i}`} match={match} />
              ))
            }
        </Cell>
        <Cell size={2} phoneSize={4}>
          <List>
            <Subheader primary primaryText="Liability" />
          </List>
          {
              liabilityTickets.map((ticket, i) => (
              <TicketDashboardCard ticket={ticket} key={`ticket-${i}`} match={match} />
              ))
            }
        </Cell>
        <Cell size={2} phoneSize={4}>
          <List>
            <Subheader primary primaryText="Damages Assessment" />
          </List>
          {
              damagesTickets.map((ticket, i) => (
              <TicketDashboardCard ticket={ticket} key={`ticket-${i}`} match={match} />
              ))
            }
        </Cell>
        <Cell size={2} phoneSize={4}>
          <List>
            <Subheader primary primaryText="Settlement Hearing" />
          </List>
          {
              settlementTickets.map((ticket, i) => (
              <TicketDashboardCard ticket={ticket} key={`ticket-${i}`} match={match} />
              ))
            }
        </Cell>
        <Cell size={2} phoneSize={4}>
          <List>
            <Subheader primary primaryText="Closed" />
          </List>
          {
              closedTickets.map((ticket, i) => (
              <TicketDashboardCard ticket={ticket} key={`ticket-${i}`} match={match} />
              ))
            }
        </Cell>
      </Grid>
    );
  }
}

const mapStateToProps = storeState => ({
  tickets: storeState.ticket.tickets,
  account: storeState.authentication.account
});

const mapDispatchToProps = { getTickets };

export default connect(mapStateToProps, mapDispatchToProps)(TicketsDashboard);
