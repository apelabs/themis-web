import * as React from 'react';
import { connect } from 'react-redux';
import { MdBlock, MdSave } from 'react-icons/lib/md';
import {
  Button,
  CardText,
  DialogContainer,
  Divider,
  TextField,
  Toolbar,
} from 'react-md';
import { reduxForm } from 'redux-form';
import { getTicket, updateTicket, createTicket, clearTicket } from '../../actions/ticketActions';
import TicketForm from './ticket-form';

export class TicketModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showModal: true,
      pageX: null, pageY: null,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.clearTicket();
    } else {
      this.props.getTicket(this.props.match.params.id);
    } 
  }

  componentWillUnmount() {
    this.props.clearTicket();
  }

  saveTicket = (values) => {
    if (this.state.isNew) {
      this.props.createTicket(values);
    } else {
      this.props.updateTicket(values);
    }
    this.handleClose();
  }

  handleClose = () => {
    this.setState({
        showModal: false
    });
    this.props.history.push('/tickets');
  }

  render() {
    const isInvalid = false;
    const { ticket, loading, updating, hide } = this.props;
    const { showModal, isNew, pageX, pageY } = this.state;
    return (
      <DialogContainer
        id="ticket-dialog"
        visible={showModal}
        fullPage
        pageX={pageX}
        pageY={pageY}
        onHide={this.hide}
        aria-labelledby="ticket-dialog"
      >
        <TicketForm 
          initialValues={ticket}
          title={isNew ? "New Ticket" : "Ticket Id: " + ticket.id}
          enableReinitialize
          handleSave={this.saveTicket}
          handleClose={this.handleClose}/>
      </DialogContainer>
    );
  }
}

const mapStateToProps = storeState => ({
  ticket: storeState.ticket.ticket,
  loading: storeState.ticket.loading,
  updating: storeState.ticket.updating
});

const mapDispatchToProps = { getTicket, updateTicket, createTicket, clearTicket };

export default connect(mapStateToProps, mapDispatchToProps)(TicketModal);
