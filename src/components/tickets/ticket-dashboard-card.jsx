import React from 'react';
import { Card, CardTitle, CardActions, CardText, Button, Badge, List, ListItem } from 'react-md';
import { Link } from 'react-router-dom';
import { MdRemoveRedEye, MdKeyboardArrowRight } from 'react-icons/lib/md';
import moment from 'moment';
import UserAvatar from '../../shared/common/UserAvatar';

let TicketDashboardCard = ({ticket, match}) => {

  return (
    <Card className="card-ticket-dashboard">
          <CardTitle title={ticket.name}></CardTitle>       
          <List>     
            <ListItem primaryText="Type" secondaryText={ticket.ticketType} />
            <ListItem primaryText="Created By" secondaryText={ticket.createdBy ? <UserAvatar user={ticket.createdBy} /> : null} />   
            <ListItem primaryText="Reference #" secondaryText={ticket.id} />
            <ListItem primaryText="Label" secondaryText={ticket.label} />            
          </List>
          <CardActions className="md-divider-border md-divider-border--top">
          <Button 
          style={{width:'50%'}}
          component={Link} to={`${match.url}tickets/${ticket.id}`}
            flat
          >
            <MdRemoveRedEye /> view
          </Button>    
          <Button
            flat
            style={{width:'50%'}}
            primary 
          >
            <MdKeyboardArrowRight/> move
          </Button>    
          </CardActions>
        </Card>
  )
}

export default TicketDashboardCard;
