import React from 'react';
import { Toolbar, Cell, Card, CardTitle, CardActions, CardText, Button, TextField } from 'react-md';
import { Form, Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import TextInput from '../../shared/form-fields/TextInput';
import TextAreaInput from '../../shared/form-fields/TextAreaInput';

let TicketForm = ({initialValues, handleSubmit, handleClose, handleSave, loginError, title}) => {

  return (
    <Form id="ticket-form" onSubmit={handleSubmit(handleSave)}>
        <Toolbar
          fixed
          colored
          title={title}
          titleId="ticket-dialog-title"
          nav={<Button icon onClick={handleClose}>close</Button>}
          actions={<Button flat type="submit">Save</Button>}
        />
        <section className="md-toolbar-relative">
          <CardText>
            <TextInput 
              type="text" 
              name="name" 
              placeholder="Name" 
              label="Name" 
              paddedBlock
              required />
            <TextInput 
              type="text" 
              name="label" 
              placeholder="Label" 
              label="Label" 
              required />
            <TextInput 
              type="text" 
              name="ticketType" 
              placeholder="Type" 
              label="Type" 
              required />
            <TextInput 
              type="text" 
              name="status" 
              placeholder="Status" 
              label="Status" 
              required />
            <TextAreaInput 
              id="ticket-desc" 
              name="description" 
              placeholder="Description" 
              label="Description" />
          </CardText>          
        </section>
    </Form> 
  )
}

TicketForm = reduxForm({
  form: 'ticket-form'
})(TicketForm)

export default TicketForm;
