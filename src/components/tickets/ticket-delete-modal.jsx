import * as React from 'react';
import { connect } from 'react-redux';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';
import { FaBan, FaTrash } from 'react-icons/lib/fa';

import { ICrudGetAction, ICrudDeleteAction } from '../../shared/model/redux-action.type';
import { getTicket, deleteTicket } from '../../actions/ticketActions';

export class TicketDeleteModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showModal: true
    };
  }

  componentDidMount() {
    this.props.getTicket(this.props.match.params.id);
  }

  confirmDelete = () => {
    this.props.deleteTicket(this.props.ticket.id);
    this.handleClose();
  }

  handleClose = () => {
    this.setState({
        showModal: false
    });
    this.props.history.push('/tickets');
  }

  render() {
    const { ticket } = this.props;
    const { showModal } = this.state;
    return (
      <Modal
        isOpen={showModal} modalTransition={{ timeout: 20 }} backdropTransition={{ timeout: 10 }}
        toggle={this.handleClose}
      >
      <ModalHeader toggle={this.handleClose}>Confirm delete operation</ModalHeader>
      <ModalBody>
        Are you sure you want to delete this Ticket: {ticket.id}?
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" onClick={this.handleClose}>
          <FaBan/>&nbsp;
          Cancel
        </Button>
        <Button color="danger" onClick={this.confirmDelete}>
          <FaTrash/>&nbsp;
          Delete
        </Button>
      </ModalFooter>
    </Modal>
    );
  }
}

const mapStateToProps = storeState => ({
  ticket: storeState.ticket.ticket
});

const mapDispatchToProps = { getTicket, deleteTicket };

export default connect(mapStateToProps, mapDispatchToProps)(TicketDeleteModal);
