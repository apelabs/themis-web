import './app.css';

import * as React from 'react';
import { connect } from 'react-redux';
import { HashRouter  as Router, Route } from 'react-router-dom';

import { NavigationDrawer } from 'react-md';
import { Card, Grid } from 'react-md';
import LoadingBar from 'react-redux-loading-bar';

import { getSession, logout } from './reducers/authentication';
import Header from './shared/layout/header/header';
import Footer from './shared/layout/footer/footer';
import NavLink from './shared/layout/drawer/NavLink';
import AppRoutes from './routes';

const navItemsGuest = [{
  label: 'Home',
  to: '/',
  exact: true,
  icon: 'home',
}, {
  label: 'Login',
  to: '/login',
  icon: 'person',
}, {
  label: 'Register',
  to: '/register',
  icon: 'person_add',
}];

const navItemsUsert = [{
  label: 'Home',
  to: '/',
  exact: true,
  icon: 'home',
}, {
  label: 'Settings',
  to: `/admin/user-management`,
  icon: 'settings',
}, {
  label: 'Tickets',
  to: `/tickets`,
  icon: 'apps',
}, {
  label: 'Logout',
  to: '/logout',
  icon: 'exit_to_app',
},];

export class App extends React.Component {
  componentDidMount() {
    this.props.getSession();
  }

  handleLogout = () => {
    this.props.logout();
  }

  render() {
    const navItems = this.props.isAuthenticated ? navItemsUsert : navItemsGuest;
    return (
      <div>
        <LoadingBar className="loading-bar" />
        <Router>        
          <Route render={({ location }) => (          
          <NavigationDrawer
          drawerTitle="Themis"
          toolbarTitle="Themis"
          mobileDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
          desktopDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT_MINI}
          navItems={navItems.map(props => <NavLink {...props} key={props.to} />)}
          contentId="app-view-container"
          contentClassName="md-grid"
          >
            
            <div id="app-view-container">
              
              <AppRoutes/>        
              <Footer/>
            </div>
          </NavigationDrawer>
          )} />
        </Router>
      </div>
    );
  }
}

const mapStateToProps = storeState => ({
  isAuthenticated: storeState.authentication.isAuthenticated,
  embedded: storeState.layout.embedded
});

const mapDispatchToProps = { getSession, logout };

export default connect(mapStateToProps, mapDispatchToProps)(App);
